package nl.hsl.service;

import nl.hsl.model.Docent;
import nl.hsl.persistence.DocentDAO;

import javax.inject.Inject;
import java.util.Collection;

/**
 * Created by Negin Nafissi on 09-11-2017.
 */
public class DocentService extends BaseService<Docent> {

    private final DocentDAO dao;

    /**
     * Instantiates a new Docent service.
     *
     * @param dao the dao
     */
    @Inject
    public DocentService(DocentDAO dao) {
        this.dao = dao;
    }

    /**
     * Get all collection.
     *
     * @return the collection
     */
    public Collection<Docent> getAllDocent() {
        return dao.getAllDocent();
    }

    /**
     * Get Docent.
     *
     * @param id the docentid
     * @return the docent
     */
    public Docent getDocent(int id) {
        return dao.getDocent(id);
    }

    /**
     * Add Docent.
     *
     * @param docent the docent
     */
    public void addDocent(Docent docent) {
        dao.addDocent(docent);
    }

    /**
     * Update Docent.
     *
     * @param docent the docent
     */
    public void updateDocent(Docent docent) {
        dao.updateDocent(docent);
    }

    /**
     * Deactive Docent.
     *
     * @param id the docentid
     */
    public void changeStatusDocent(int id) {
        dao.changeStatusDocent(id);
    }


}
