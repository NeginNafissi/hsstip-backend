package nl.hsl.persistence;


import nl.hsl.service.DatabaseService;
import javax.inject.Singleton;
import java.sql.Connection;

/**
 * The type Database dao.
 *
 * @author Negin Nafissi
 */
public abstract class DatabaseDAO {

    /**
     * The Conn.
     */
    @Singleton
    protected Connection conn;

    /**
     * Instantiates a new Database dao.
     *
     * @throws Exception the exception
     */
    public DatabaseDAO() throws Exception {
        this.conn = DatabaseService.getInstance().getConnection("postgres", "admin");

    }
}
