package nl.hsl.persistence;

import nl.hsl.model.Docent;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Negin Nafissi
 */
public class DocentDAO extends DatabaseDAO {


    private PreparedStatement getDocent;
    private PreparedStatement addDocent;
    private PreparedStatement updateDocent;
    private PreparedStatement changeStatusDocent;
    private PreparedStatement getAllDocent;


    /**
     * Instantiates a new Docent dao.
     *
     * @throws Exception the exception
     */
    public DocentDAO() throws Exception {

        super();
        prepareStatements();

    }

    private void prepareStatements() {

        try {
            getDocent = conn.prepareStatement("SELECT * FROM docent WHERE docentid=?");
            addDocent = conn.prepareStatement("INSERT INTO docent (voornaam, tussenvoegsel, achternaam, email, " +
                            "telefoonnummer, gebruikersnaam, wachtwoord, rol, status) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
            getAllDocent = conn.prepareStatement("SELECT * FROM docent");
            updateDocent = conn.prepareStatement("UPDATE docent SET voornaam=?, tussenvoegsel=?, achternaam=?," +
                    " email=?, telefoonnummer=?, gebruikersnaam=?, wachtwoord=?, rol=? WHERE docentid=?");
            changeStatusDocent = conn.prepareStatement("UPDATE docent SET status=? WHERE docentid =?");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * Add docent.
     *
     * @param docent the docent
     */
    public void addDocent(Docent docent) {

        try {
            addDocent.setString(1, docent.getVoornaam());
            addDocent.setString(2, docent.getTussenvoegsel());
            addDocent.setString(3, docent.getAchternaam());
            addDocent.setString(4, docent.getEmail());
            addDocent.setInt(5, docent.getTelefoonnummer());
            addDocent.setString(6, docent.getGebruikersnaam());
            addDocent.setString(7, docent.getWachtwoord());
            addDocent.setBoolean(8, docent.getRol());
            addDocent.setBoolean(9, docent.getStatus());

            addDocent.executeUpdate();

            //Sets the auto generated id of the docent.
            ResultSet rs = addDocent.getGeneratedKeys();
            if (rs.next()) {
                docent.setDocentid(rs.getInt("docentid"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Get all list.
     *
     * @return the list
     */
    public List<Docent> getAllDocent() {

        List<Docent> docenten = new ArrayList<>();

        try {
            ResultSet rs = getAllDocent.executeQuery();

            while (rs.next()) {
                Docent docent = new Docent();

                docent.setDocentid(rs.getInt(1));
                docent.setVoornaam(rs.getString(2));
                docent.setTussenvoegsel(rs.getString(3));
                docent.setAchternaam(rs.getString(4));
                docent.setEmail(rs.getString(5));
                docent.setTelefoonnummer(rs.getInt(6));
                docent.setGebruikersnaam(rs.getString(7));
                docent.setWachtwoord(rs.getString(8));
                docent.setRol(rs.getBoolean(9));
                docent.setStatus(rs.getBoolean(10));

                docenten.add(docent);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return docenten;
    }

    /**
     * Gets docent.
     *
     * @param email the email
     * @return the docent
     */
    public Docent getDocent(int id) {

        Docent docent = new Docent();

        try {
            ResultSet rs = getDocent.executeQuery();

            while (rs.next()) {
                docent.setDocentid(rs.getInt(1));
                docent.setVoornaam(rs.getString(2));
                docent.setTussenvoegsel(rs.getString(3));
                docent.setAchternaam(rs.getString(4));
                docent.setEmail(rs.getString(5));
                docent.setTelefoonnummer(rs.getInt(6));
                docent.setGebruikersnaam(rs.getString(7));
                docent.setWachtwoord(rs.getString(8));
                docent.setRol(rs.getBoolean(9));
                docent.setStatus(rs.getBoolean(10));

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return docent;
    }

    /**
     * Update.
     *
     * @param docent the docent
     */
    public void updateDocent(Docent docent) {

        try {
            updateDocent.setString(1, docent.getVoornaam());
            updateDocent.setString(2, docent.getTussenvoegsel());
            updateDocent.setString(3, docent.getAchternaam());
            updateDocent.setString(4, docent.getEmail());
            updateDocent.setInt(5, docent.getTelefoonnummer());
            updateDocent.setString(6, docent.getGebruikersnaam());
            updateDocent.setString(7, docent.getWachtwoord());
            updateDocent.setBoolean(8, docent.getRol());
            updateDocent.setBoolean(9, docent.getStatus());

            updateDocent.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Change status.
     *
     * @param id the id
     */
    public void changeStatusDocent(int id) {

        try {
            if (getDocent(id).getStatus() == true) {
                changeStatusDocent.setBoolean(1, false);
                changeStatusDocent.execute();

            } else {
                changeStatusDocent.setBoolean(1, true);
                changeStatusDocent.execute();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
