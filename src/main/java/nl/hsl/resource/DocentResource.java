package nl.hsl.resource;

import com.fasterxml.jackson.annotation.JsonView;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import io.dropwizard.auth.Auth;
import nl.hsl.View;
import nl.hsl.model.Docent;
import nl.hsl.model.User;
import nl.hsl.service.DocentService;

import javax.annotation.security.PermitAll;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Collection;

/**
 * Created by Negin Nafissi on 09-11-2017.
 */
@Singleton
@Path("/docent")
@Produces(MediaType.APPLICATION_JSON)
public class DocentResource {

    private final DocentService service;

    /**
     * Instantiates a new Docent resource.
     *
     * @param service the service
     */
    @Inject
    public DocentResource(DocentService service) {
        this.service = service;
    }

    /**
     * Retrieve all collection.
     *
     * @return the collection
     */
    @GET
    @JsonView(View.Public.class)
    @PermitAll
    public Collection<Docent> retrieveAll() {
        return service.getAllDocent();
    }

    /**
     * Retrieve docent.
     *
     * @param id the id
     * @return the employee
     */
    @GET
    @Path("/{id}")
    @JsonView(View.Public.class)
    public Docent retrieve(@PathParam("id") int id) {
        return service.getDocent(id);
    }

    /**
     * Create.
     *
     * @param docent the docent
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @JsonView(View.Protected.class)
    public void create(Docent docent) {
        service.addDocent(docent);
    }

    /**
     * Update.
     *
     * @param id            the docentid
     * @param authenticator the authenticator
     * @param docent      the docent
     */
    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @JsonView(View.Protected.class)
    @PermitAll
    public void update(@PathParam("id") int id, @Auth User authenticator, Docent docent) {
        service.updateDocent(docent);
    }

    /**
     * Deactive.
     *
     * @param id the id
     */
    @DELETE
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @JsonView(View.Protected.class)
    @PermitAll
    public void changeStatus(@PathParam("id") int id) {
        service.changeStatusDocent(id);
    }
}